#!/bin/bash
##########################################################
# This script will be called as part of container exec
# the script essentially traverse to the dbt project folder
# and executes the run script.
#
##########################################################

PARAM_RUN_SCRIPT="$1"

echo " ------------ RUN_PIPELINE.SH ------------------------- "

if [ -z "$PARAM_RUN_SCRIPT" ]
then
	echo " Error : PARAM_RUN_SCRIPT is not initialized."
	echo " Current dir : $PWD"
	ls -l
	echo " Exiting."
	exit -1
fi

# Set the execute bit for all script to avoid any issues
chmod 750 $PARAM_RUN_SCRIPT

# Find the directory in which the script is residing.
# ASSUMPTION : The script directory is the dbt_profile dir.
#
#DBT_PROJECT_DIR=$( dirname "$PARAM_RUN_SCRIPT" )
#echo "DBT Project folder : $DBT_PROJECT_DIR"
# cd $DBT_PROJECT_DIR
# HOME_DIR=${PWD}
# chmod 750 *.sh
# Initialize secrets variable
#. /dbt_run_init.sh

echo " Performing run : $PARAM_RUN_SCRIPT ..."
"$PARAM_RUN_SCRIPT"

echo " ------------ RUN_PIPELINE.SH ------------------------- "
echo " Finished!!! "
