

resource "azurerm_container_group" "acg_dbt" {
  depends_on = [
    azurerm_container_registry.acr
  ]
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  name = join("", [
    local.name_prefix,
    "aci01"
  ])
  tags = var.tags_base

  restart_policy = "Never"
  os_type        = "Linux"

  # Though this is not needed, its a current bug in the provider. we are 
  # opening up a dummy port for now. may be in the future this will get fixed.
  ip_address_type = "Public"

  identity {
    type = "UserAssigned"
    identity_ids = [
      azurerm_user_assigned_identity.uai.id
    ]
  }


  image_registry_credential {
    server   = azurerm_container_registry.acr.login_server
    username = data.azurerm_client_config.current.client_id
    password = var.spsecret
  }

  container {
    name   = "dbtonaz"
    image  = "${azurerm_container_registry.acr.login_server}/${var.dbt_docker_tag}:latest"
    cpu    = 1
    memory = 1
    environment_variables = {
      "ENV_AZSUB"        = data.azurerm_client_config.current.subscription_id
      "ENV_RG"           = azurerm_resource_group.rg.name
      "ENV_KV_URL"       = azurerm_key_vault.kv.vault_uri
      "ENV_KV_SFLK"      = azurerm_key_vault_secret.kvs_sflk_conn.name
      "ENV_DBT_PACKAGED" = "/code/dbtoncloud.zip"
      "ENV_DBT_RUN_CMD"  = "dbtdataops/dbtoncloud/dbt_datapipeline.sh"
    }

    volume {
      name                 = "code"
      mount_path           = "/code/"
      read_only            = true
      storage_account_name = azurerm_storage_account.adls2.name
      storage_account_key  = azurerm_storage_account.adls2.primary_access_key
      share_name           = azurerm_storage_share.share_code.name
    }

    #Date : 8-dec-2020
    # current bug : https://github.com/terraform-providers/terraform-provider-azurerm/issues/1697
    # We technically do not need a port open, but due to limitation 
    # in the current provider implementation, we are forced to create a dummy port
    # as a work around.
    ports {
      port     = 9999
      protocol = "UDP"
    }
  }
}

#
# Zip the dbt project and upload the same to storage account share
#
resource "null_resource" "zip_code_upload" {
  provisioner "local-exec" {
    command = <<ZIP_UPLOAD_EOF
      echo "archiving dbt project ..."
      zip ../dbtdataops/dbtoncloud.zip ../dbtdataops/dbtoncloud/*

      echo "uploading to file share ..."
      az storage file upload -s $code_shr \
        --source ../dbtdataops/dbtoncloud.zip \
        --account-name $sa \
        --account-key $sa_key
    ZIP_UPLOAD_EOF

    environment = {
      code_shr = azurerm_storage_share.share_code.name
      sa       = azurerm_storage_account.adls2.name
      sa_key   = azurerm_storage_account.adls2.primary_access_key
    }
  }
}

